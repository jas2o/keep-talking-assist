﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombMorseCode {

        #region Dictionaries
        private static Dictionary<string, string> response = new Dictionary<string, string> {
            { "SHELL", "3.505" },
            { "HALLS", "3.515" },
            { "SLICK", "3.522" },
            { "TRICK", "3.532" },
            { "BOXES", "3.535" },
            { "LEAKS", "3.542" },
            { "STROBE", "3.545" },
            { "BOMBS", "3.565" },
            { "STEAK", "3.582" },
            { "BEATS", "3.600" }
        };

        private static Dictionary<string, string> morse = new Dictionary<string, string> {
            { "A", ".-" },
            { "B", "-..." },
            { "C", "-.-." },
            { "D", "-.." },
            { "E", "." },
            { "F", "..-." },
            { "G", "--." },
            { "H", "...." },
            { "I", ".." },
            { "J", ".---" },
            { "K", "-.-" },
            { "L", ".-.." },
            { "M", "--" },
            { "N", "-." },
            { "O", "---" },
            { "P", ".--." },
            { "Q", "--.-" },
            { "R", ".-." },
            { "S", "..." },
            { "T", "-" },
            { "U", "..-" },
            { "V", "...-" },
            { "W", ".--" },
            { "X", "-..-" },
            { "Y", "-.--" },
            { "Z", "--.." }
        };
        #endregion

        public static void Start() {
            TTS.Speak("Morse");

            string given = "";

            bool loop = true;
            while (loop) {
                string input = Console.ReadLine();

                if (input == "CLEAR" || input == "RESET")
                    given = "";
                else {

                    string letter = morse.FirstOrDefault(x => x.Value == input).Key;

                    given += letter;
                    TTS.Speak(letter);

                    //Rotate given, see if there's a word?

                    List<string> possible = new List<string>();
                    foreach (KeyValuePair<string, string> entry in response) {
                        if (entry.Key.Contains(given))
                            possible.Add(entry.Key);
                    }

                    if (possible.Count == 1) {
                        TTS.Speak(possible[0]);
                        TTS.Speak(response[possible[0]]);
                        loop = false;
                    }
                }
            }
        }

    }
}
