﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombSimon {

        private static Dictionary<string, string[]> SimonVowel = new Dictionary<string, string[]> {
            { "Red", new string[] { "Blue", "Yellow", "Green" } },
            { "Blue",  new string[] { "Red", "Green", "Red" } },
            { "Green",  new string[] { "Yellow", "Blue", "Yellow" } },
            { "Yellow",  new string[] { "Green", "Red", "Blue" } }
        };

        private static Dictionary<string, string[]> SimonNot = new Dictionary<string, string[]> {
            { "Red", new string[] { "Blue", "Red", "Yellow" } },
            { "Blue",  new string[] { "Yellow", "Blue", "Green" } },
            { "Green",  new string[] { "Green", "Yellow", "Blue" } },
            { "Yellow",  new string[] { "Red", "Green", "Red" } }
        };

        public static void Start() {
            //TTS.Speak("Simon.");

            if (Program.usingVoice) {
                TTS.Speak("Voice input not supported.");
            } else {
                bool loop = true;
                while (loop) {
                    TTS.Speak("Simon.");
                    string[] words = TitleCaseWords(Console.ReadLine());

                    if (words[0].ToUpper() == "DONE") {
                        loop = false;
                    } else if (words[0].ToUpper() == "STRIKE") {
                        int num = Int32.Parse(words[1]);
                        if (num >= 0 && num <= 2) {
                            Bomb.Strikes = num;
                            TTS.Speak(num + " strikes.");
                        }
                    } else {
                        string build = "";
                        foreach(string word in words) {
                            if (SimonVowel.ContainsKey(word)) {
                                if (Bomb.SerialVowel == Bomb.ID.Yes) {
                                    build += SimonVowel[word][Bomb.Strikes] + " ";
                                } else {
                                    build += SimonNot[word][Bomb.Strikes] + " ";
                                }
                            } else
                                build += "Fail ";
                        }
                        TTS.Speak(build);
                    }
                }
            }
        }

        private static string[] TitleCaseWords(string input) {
            input = input.ToLower();
            string[] word = input.Split(' ');

            for(int i = 0; i < word.Length; i++)
                word[i] = word[i][0].ToString().ToUpper() + word[i].Substring(1);

            return word;
        }
    }
}
