﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombWiresSequence {

        private static Dictionary<int, bool[]> red = new Dictionary<int, bool[]> {
            { 0, new bool[] { false, false, true } },
            { 1, new bool[] { false, true, false } },
            { 2, new bool[] { true, false, false } },
            { 3, new bool[] { true, false, true } },
            { 4, new bool[] { false, true, false } },
            { 5, new bool[] { true, false, true } },
            { 6, new bool[] { true, true, true } },
            { 7, new bool[] { true, true, false } },
            { 8, new bool[] { false, true, false } }
        };

        private static Dictionary<int, bool[]> blue = new Dictionary<int, bool[]> {
            { 0, new bool[] { false, true, false } },
            { 1, new bool[] { true, false, true } },
            { 2, new bool[] { false, true, false } },
            { 3, new bool[] { true, false, false } },
            { 4, new bool[] { false, true, false } },
            { 5, new bool[] { false, true, true } },
            { 6, new bool[] { false, false, true } },
            { 7, new bool[] { true, false, true } },
            { 8, new bool[] { true, false, false } }
        };

        private static Dictionary<int, bool[]> black = new Dictionary<int, bool[]> {
            { 0, new bool[] { true, true, true } },
            { 1, new bool[] { true, false, true } },
            { 2, new bool[] { false, true, false } },
            { 3, new bool[] { true, false, true } },
            { 4, new bool[] { false, true, false } },
            { 5, new bool[] { false, true, true } },
            { 6, new bool[] { true, true, false } },
            { 7, new bool[] { false, false, true } },
            { 8, new bool[] { false, false, true } }
        };

        public static void Start() {
            TTS.Speak("SEQUENCE.");

            int numRed = 0;
            int numBlue = 0;
            int numBlack = 0;

            bool loop = true;
            while (loop) {
                TTS.Speak("Next");
                string input = Console.ReadLine().ToUpper();

                if (input == "DONE" || input == "EXIT")
                    return;

                bool[] toCut = new bool[3];
                string[] word = input.Split(' ');
                string color = word[0];
                string letter = word.Last()[0].ToString();

                if (letter != "A" && letter != "B" && letter != "C")
                    continue;

                if (color == "RED")
                    toCut = red[numRed++];
                else if (color == "BLUE")
                    toCut = blue[numBlue++];
                else if (color == "BLACK")
                    toCut = black[numBlack++];
                else
                    continue;

                if ((letter == "A" && toCut[0]) || (letter == "B" && toCut[1]) || (letter == "C" && toCut[2]))
                    TTS.Speak("Cut");
                else
                    TTS.Speak("Leave");
            }
        }

    }
}
