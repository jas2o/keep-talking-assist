﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombPassword {

        private static string[] valid = new string[] {
                "ABOUT", "AFTER", "AGAIN", "BELOW", "COULD",
                "EVERY", "FIRST", "FOUND", "GREAT", "HOUSE",
                "LARGE", "LEARN", "NEVER", "OTHER", "PLACE",
                "PLANT", "POINT", "RIGHT", "SMALL", "SOUND",
                "SPELL", "STILL", "STUDY", "THEIR", "THERE",
                "THESE", "THING", "THINK", "THREE", "WATER",
                "WHERE", "WHICH", "WORLD", "WOULD", "WRITE"};

        private static char[][] letters;

        public static void Start() {
            TTS.Speak("Password.");

            bool loop = true;
            int col = 1;

            letters = new char[5][];
            for (int i = 0; i < 5; i++) {
                letters[i] = new char[6];
            }

            if (Program.usingVoice) {
                TTS.Speak("Voice input not supported.");
            } else {
                while (loop) {
                    TTS.Speak("Column " + col);
                    string input = Console.ReadLine();

                    if (input == "EXIT" || input == "DONE") {
                        loop = false;
                        return;
                    } else if (input == "1" || input == "2" || input == "3" || input == "4" || input == "5") {
                        col = Int32.Parse(input);
                    } else {
                        input = input.Replace(" ", "").ToUpper();
                        if (input.Length == 6) {
                            for (int i = 0; i < 6; i++)
                                letters[col - 1][i] = input[i];
                        }
                        if (Test())
                            loop = false;

                        col++;
                        if (col >= 6)
                            col = 1;
                    }
                }
            }
        }

        private static bool Test() {
            List<string> possible = new List<string>();
            foreach (string word in valid) {
                bool allow = true;
                for (int c = 0; c < 5; c++) {
                    if (letters[c][0] != '\0' && !letters[c].Contains(word[c])) {
                        allow = false;
                        break;
                    }
                }
                if (allow)
                    possible.Add(word);
            }

            if (possible.Count == 1) {
                TTS.Speak("The password is " + possible[0]);
                return true;
            } else if(possible.Count > 1 && possible.Count < 3)
                TTS.Speak("Could be " + string.Join(", ", possible));

            return false;
        }
    }
}
