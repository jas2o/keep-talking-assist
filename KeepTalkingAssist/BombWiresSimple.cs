﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombWiresSimple {

        enum WireColor { Red, Blue, Yellow, White, Black };

        public static void Start() {

            TTS.Speak("Wires.");
            string build = "";
            WireSimple[] wires = null;

            if (Program.usingVoice) {
                using (SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"))) {
                    recognizer.SetInputToDefaultAudioDevice();

                    Choices choicesColours = new Choices("Done");
                    foreach (string color in Enum.GetNames(typeof(WireColor)))
                        choicesColours.Add(color);
                    GrammarBuilder grammarBuilder = new GrammarBuilder(choicesColours);
                    Grammar grammar = new Grammar(grammarBuilder);
                    recognizer.LoadGrammar(grammar);

                    int k = 0;
                    bool loop = true;
                    RecognitionResult rr = null;
                    while (loop) {
                        TTS.Speak("Colour");
                        rr = recognizer.Recognize();
                        if (rr != null) {
                            if (rr.Text.ToUpper() == "DONE") {
                                loop = false;
                            } else {
                                TTS.Speak(rr.Text);
                                build += rr.Text + " ";

                                k++;
                                if (k == 6)
                                    loop = false;
                            }
                        }
                    }
                }
            } else {
                int k = 0;
                bool loop = true;
                while (loop) {
                    TTS.Speak("Colour");
                    string input = Console.ReadLine().ToLower();
                    input = input[0].ToString().ToUpper() + input.Substring(1);

                    if (input.ToUpper() == "DONE") {
                        loop = false;
                    } else if(Enum.IsDefined(typeof(WireColor), input)) {
                        //TTS.Speak(input);
                        build += input.ToUpper() + " ";

                        k++;
                        if (k == 6)
                            loop = false;
                    }
                }
            }

            //--
            string[] words = build.Trim().ToUpper().Split(' ');
            wires = new WireSimple[words.Length];
            int i = 0;
            foreach (string w in words) {
                switch (w) {
                    case "RED": wires[i++] = new WireSimple(WireColor.Red); break;
                    case "BLUE": wires[i++] = new WireSimple(WireColor.Blue); break;
                    case "YELLOW": wires[i++] = new WireSimple(WireColor.Yellow); break;
                    case "WHITE": wires[i++] = new WireSimple(WireColor.White); break;
                    case "BLACK": wires[i++] = new WireSimple(WireColor.Black); break;
                }
            }

            string readcolours = "";
            foreach(WireSimple ws in wires)
                readcolours += ws.Color.ToString() + " ";
            TTS.Speak("Again: " + readcolours);

            int Last = wires.Length - 1;

            if (wires.Length == 3) {
                if (wires.Count(w => w.Color == WireColor.Red) == 0) {
                    TTS.Speak("Cut the second wire.");
                } else if (wires[Last].Color == WireColor.White) {
                    TTS.Speak("Cut the last wire.");
                } else if (wires.Count(w => w.Color == WireColor.Blue) > 1) {
                    TTS.Speak("Cut the last blue wire.");
                } else {
                    TTS.Speak("Cut the last wire.");
                }
            } else if (wires.Length == 4) {
                if (wires.Count(w => w.Color == WireColor.Red) > 1 && Bomb.SerialLast == Bomb.Serial.Odd) {
                    TTS.Speak("Cut the last wire.");
                } else if (wires[Last].Color == WireColor.Yellow && wires.Count(w => w.Color == WireColor.Red) == 0) {
                    TTS.Speak("Cut the first wire.");
                } else if (wires.Count(w => w.Color == WireColor.Blue) == 1) {
                    TTS.Speak("Cut the first wire.");
                } else if (wires.Count(w => w.Color == WireColor.Yellow) > 1) {
                    TTS.Speak("Cut the last wire.");
                } else {
                    TTS.Speak("Cut the second wire.");
                }
            } else if (wires.Length == 5) {
                if (wires[Last].Color == WireColor.Black && Bomb.SerialLast == Bomb.Serial.Odd) {
                    TTS.Speak("Cut the fourth wire.");
                } else if(wires.Count(w => w.Color == WireColor.Red) == 1 && wires.Count(w => w.Color == WireColor.Yellow) > 1) {
                    TTS.Speak("Cut the first wire.");
                } else if(wires.Count(w => w.Color == WireColor.Black) == 0) {
                    TTS.Speak("Cut the second wire.");
                } else {
                    TTS.Speak("Cut the first wire.");
                }
            } else if (wires.Length == 6) {
                if(wires.Count(w => w.Color == WireColor.Yellow) == 0 && Bomb.SerialLast == Bomb.Serial.Odd) {
                    TTS.Speak("Cut the third wire.");
                } else if(wires.Count(w => w.Color == WireColor.Yellow) == 1 && wires.Count(w => w.Color == WireColor.White) > 1) {
                    TTS.Speak("Cut the fourth wire.");
                } else if(wires.Count(w => w.Color == WireColor.Red) == 0) {
                    TTS.Speak("Cut the last wire.");
                } else {
                    TTS.Speak("Cut the fourth wire.");
                }
            } else {
                TTS.Speak("Error");
            }

        }

        private class WireSimple {
            public WireColor Color;
            public WireSimple(WireColor color) {
                Color = color;
            }
        }
    }
}
