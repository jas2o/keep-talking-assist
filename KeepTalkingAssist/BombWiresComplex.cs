﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombWiresComplex {

        private static List<WiresComplex> lookup = new List<WiresComplex>() {
            new WiresComplex(true, true, true, true, "D"),
            new WiresComplex(true, true, true, false, "P"),
            new WiresComplex(true, true, false, true, "S"),
            new WiresComplex(true, true, false, false, "S"),
            new WiresComplex(true, false, true, true, "B"),
            new WiresComplex(true, false, true, false, "C"),
            new WiresComplex(true, false, false, true, "B"),
            new WiresComplex(true, false, false, false, "S"),
            new WiresComplex(false, true, true, true, "P"),
            new WiresComplex(false, true, true, false, "D"),
            new WiresComplex(false, true, false, true, "P"),
            new WiresComplex(false, true, false, false, "S"),
            new WiresComplex(false, false, true, true, "B"),
            new WiresComplex(false, false, true, false, "C"),
            new WiresComplex(false, false, false, true, "D"),
            new WiresComplex(false, false, false, false, "C"),
        };

        public static void Start() {
            TTS.Speak("Complicated.");

            bool[] red = new bool[6];
            bool[] blue = new bool[6];
            bool[] led = new bool[6];
            bool[] symbol = new bool[6];

            int i = 0;
            bool loop = true;
            while (loop) {
                TTS.Speak("Wire");
                string input = Console.ReadLine().ToUpper();

                if (input == "DONE") {
                    loop = false;
                } else {
                    string[] word = input.Split(' ');
                    if (word.Length > 1) {
                        red[i] = (word[1] == "RED");
                        blue[i] = (word[1] == "BLUE");
                    }
                    red[i] = (word[0] == "RED");
                    blue[i] = (word[0] == "BLUE");

                    i++;
                    if (i == 6)
                        loop = false;
                }
            }

            if (i > 0) {
                TTS.Speak("Lights");
                string[] inputLED = Console.ReadLine().ToUpper().Split(' ');
                for (int k = 0; k < i; k++)
                    if (inputLED[k] == "ON" || inputLED[k] == "YES")
                        led[k] = true;

                TTS.Speak("Stars");
                string[] inputSymbol = Console.ReadLine().ToUpper().Split(' ');
                for (int k = 0; k < i; k++)
                    if (inputSymbol[k] == "ON" || inputSymbol[k] == "YES")
                        symbol[k] = true;

                //----

                List<string> build = new List<string>();
                for(int k = 0; k < i; k++) {
                    WiresComplex found = lookup.FirstOrDefault(w => w.Red == red[k] && w.Blue == blue[k] && w.Star == symbol[k] && w.LED == led[k]);
                    if (found.Result == "C" || (found.Result == "P" && Bomb.ParallelPort == Bomb.ID.Yes) || (found.Result == "S" && Bomb.SerialLast == Bomb.Serial.Even) || (found.Result == "B" && Bomb.Batteries > 1))
                        build.Add("Cut");
                    else
                        build.Add("Leave");
                }

                TTS.Speak(string.Join<string>(", ", build));
            }
        }

        private class WiresComplex {
            public bool Red;
            public bool Blue;
            public bool Star;
            public bool LED;
            public string Result;

            public WiresComplex(bool red, bool blue, bool star, bool led, string result) {
                Red = red;
                Blue = blue;
                Star = star;
                LED = led;
                Result = result;
            }
        }
    }
}
