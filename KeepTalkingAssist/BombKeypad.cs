﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombKeypad {

        private static Dictionary<int, string[]> symbols = new Dictionary<int, string[]> {
            {0, new string[] { "O", "BALLON" } },
            {1, new string[] { "A", "AT", "PYRAMID" } },
            {2, new string[] { "LAMBDA" } },
            {3, new string[] { "LIGHTNING" } },
            {4, new string[] { "OCTOPUS", "SQUID", "SPIDER", "SPACESHIP" } },
            {5, new string[] { "H", "HY", "XY" } },
            {6, new string[] { "BACKWARDS C", "C BACKWARDS", "C REVERSE", "C INVERSE",  "C FLIPPED" } },
            {7, new string[] { "E", "FISH" } },
            {8, new string[] { "LOOP", "CURL" } },
            {9, new string[] { "WHITE STAR", "STAR WHITE" } },
            {10, new string[] { "QUESTION MARK", "QUESTION" } },
            {11, new string[] { "COPYRIGHT" } },
            {12, new string[] { "NOSE" } },
            {13, new string[] { "MIRROR", "IK" } },
            {14, new string[] { "THREE", "HALF THREE" } },
            {15, new string[] { "SIX" } },
            {16, new string[] { "PARAGRAPH", "P" } },
            {17, new string[] { "B", "BT" } },
            {18, new string[] { "SMILEY", "FACE" } },
            {19, new string[] { "TRIDENT", "FORK", "CANDELABRRA"} },
            {20, new string[] { "C", "C DOT", "C SQUARE", "C NORMAL" } },
            {21, new string[] { "SNAKE", "RABBIT" } },
            {22, new string[] { "BLACK STAR", "STAR BLACK" } },
            {23, new string[] { "EQUALS", "NOT EQUAL", "HASH", "PLUS PLUS" } },
            {24, new string[] { "AE", "A E" } },
            {25, new string[] { "N", "HALO", "BULL" } },
            {26, new string[] { "OMEGA", "PHYSICS", "SCIENCE" } }
        };


        private static int[] col1 = new int[] { 0, 1, 2, 3, 4, 5, 6 };
        private static int[] col2 = new int[] { 7, 0, 6, 8, 9, 5, 10 };
        private static int[] col3 = new int[] { 11, 12, 8, 13, 14, 2, 9 };
        private static int[] col4 = new int[] { 15, 16, 17, 4, 13, 10, 18 };
        private static int[] col5 = new int[] { 19, 18, 17, 20, 16, 21, 22 };
        private static int[] col6 = new int[] { 15, 7, 23, 24, 19, 25, 26 };

        public static void Start() {
            // Next to do would be to use the same description as what the user gave.

            TTS.Speak("KEYPAD.");

            int[] button = new int[4];
            int known = 0;
            while (known < 4) {
                TTS.Speak("SYMBOL " + (known + 1));
                string input = Console.ReadLine().ToUpper();

                if (symbols.Values.SelectMany(lst => lst).Any(name => name == input)) {
                    //Should also check it's not already in button
                    button[known++] = symbols.Keys.Where(key => symbols[key].Contains(input)).FirstOrDefault();
                }
            }

            int[][] inter = new int[6][] {
                col1.Intersect(button).ToArray(),
                col2.Intersect(button).ToArray(),
                col3.Intersect(button).ToArray(),
                col4.Intersect(button).ToArray(),
                col5.Intersect(button).ToArray(),
                col6.Intersect(button).ToArray()
            };

            foreach(int[] group in inter) {
                if(group.Length == 4) {
                    TTS.Speak("PRESS " + symbols[group[0]][0] + ", "
                        + symbols[group[1]][0] + ", "
                        + symbols[group[2]][0] + ", "
                        + symbols[group[3]][0]);
                    return;
                }
            }

            TTS.Speak("Wasn't four.");
        }

    }
}
