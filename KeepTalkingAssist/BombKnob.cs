﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombKnob {

        public static void Start() {
            TTS.Speak("KNOB.");

            bool[] Up1 = new bool[] { false, false, true, false, false, true };
            bool[] Up2 = new bool[] { false, false, true, false, true, false };
            bool[] Down1 = new bool[] { false, true, true, false, false, true };
            bool[] Down2 = new bool[] { false, false, false, false, false, false };
            bool[] Left = new bool[] { false, false, false, false, true, false };
            bool[] Right1 = new bool[] { true, false, true, false, true, false };
            bool[] Right2 = new bool[] { true, false, true, false, false, false };

            bool[] pairs = new bool[6];
            TTS.Speak("Pairs");
            string[] inputPairs = Console.ReadLine().ToUpper().Split(' ');
            for (int k = 0; k < 6; k++)
                if (inputPairs[k] == "ON" || inputPairs[k] == "YES")
                    pairs[k] = true;

            if(pairs.SequenceEqual(Up1) || pairs.SequenceEqual(Up2))
                TTS.Speak("Up");
            else if (pairs.SequenceEqual(Down1) || pairs.SequenceEqual(Down2))
                TTS.Speak("Down");
            else if (pairs.SequenceEqual(Left))
                TTS.Speak("Left");
            else if (pairs.SequenceEqual(Right1) || pairs.SequenceEqual(Right2))
                TTS.Speak("Right");
        }

    }
}
