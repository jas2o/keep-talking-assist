﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombMaze {

        //https://github.com/devanhurst/ktane_voice/blob/master/solvers/mazes.rb
        private static Dictionary<string, char[,]> mazes = new Dictionary<string, char[,]>() {
            //XYXY - # are indicators, X are walls.
            {"1263" , new char[,]{
                //1       2       3       4       5       6
                {'*',' ','*',' ','*','X','*',' ','*',' ','*'},
                {' ','X','X','X',' ','X',' ','X','X','X','X'},
                {'#','X','*',' ','*','X','*',' ','*',' ','*'},
                {' ','X',' ','X','X','X','X','X','X','X',' '},
                {'*','X','*',' ','*','X','*',' ','*',' ','#'},
                {' ','X','X','X',' ','X',' ','X','X','X',' '},
                {'*','X','*',' ','*',' ','*','X','*',' ','*'},
                {' ','X','X','X','X','X','X','X','X','X',' '},
                {'*',' ','*',' ','*','X','*',' ','*','X','*'},
                {' ','X','X','X',' ','X',' ','X','X','X',' '},
                {'*',' ','*','X','*',' ','*','X','*',' ','*'}
            }},
            {"2452" , new char[,]{
                //1       2       3       4       5       6
                {'*',' ','*',' ','*','X','*',' ','*',' ','*'},
                {'X','X',' ','X','X','X',' ','X',' ','X','X'},
                {'*',' ','*','X','*',' ','*','X','#',' ','*'},
                {' ','X','X','X',' ','X','X','X','X','X',' '},
                {'*','X','*',' ','*','X','*',' ','*',' ','*'},
                {' ','X',' ','X','X','X',' ','X','X','X',' '},
                {'*',' ','#','X','*',' ','*','X','*','X','*'},
                {' ','X','X','X',' ','X','X','X',' ','X',' '},
                {'*','X','*','X','*','X','*',' ','*','X','*'},
                {' ','X',' ','X',' ','X',' ','X','X','X',' '},
                {'*','X','*',' ','*','X','*',' ','*',' ','*'}}
            },
            {"4464" , new char[,]{
                //1       2       3       4       5       6
                {'*',' ','*',' ','*','X','*','X','*',' ','*'},
                {' ','X','X','X',' ','X',' ','X',' ','X',' '},
                {'*','X','*','X','*','X','*',' ','*','X','*'},
                {'X','X',' ','X',' ','X','X','X','X','X',' '},
                {'*',' ','*','X','*','X','*',' ','*','X','*'},
                {' ','X',' ','X',' ','X',' ','X',' ','X',' '},
                {'*','X','*','X','*','X','#','X','*','X','#'},
                {' ','X',' ','X',' ','X',' ','X',' ','X',' '},
                {'*','X','*',' ','*','X','*','X','*','X','*'},
                {' ','X','X','X','X','X',' ','X',' ','X',' '},
                {'*',' ','*',' ','*',' ','*','X','*',' ','*'}}
            },
            {"1114" , new char[,]{
                //1       2       3       4       5       6
                {'#',' ','*','X','*',' ','*',' ','*',' ','*'},
                {' ','X',' ','X','X','X','X','X','X','X',' '},
                {'*','X','*','X','*',' ','*',' ','*',' ','*'},
                {' ','X',' ','X',' ','X','X','X','X','X',' '},
                {'*','X','*',' ','*','X','*',' ','*','X','*'},
                {' ','X','X','X','X','X',' ','X','X','X',' '},
                {'#','X','*',' ','*',' ','*',' ','*',' ','*'},
                {' ','X','X','X','X','X','X','X','X','X',' '},
                {'*',' ','*',' ','*',' ','*',' ','*','X','*'},
                {' ','X','X','X','X','X','X','X',' ','X',' '},
                {'*',' ','*',' ','*','X','*',' ','*','X','*'}}
            },
            {"4653" , new char[,]{
                //1       2       3       4       5       6
                {'*',' ','*',' ','*',' ','*',' ','*',' ','*'},
                {'X','X','X','X','X','X','X','X',' ','X',' '},
                {'*',' ','*',' ','*',' ','*',' ','*','X','*'},
                {' ','X','X','X','X','X',' ','X','X','X','X'},
                {'*',' ','*','X','*',' ','*','X','#',' ','*'},
                {' ','X',' ','X','X','X','X','X',' ','X',' '},
                {'*','X','*',' ','*',' ','*','X','*','X','*'},
                {' ','X','X','X','X','X',' ','X','X','X',' '},
                {'*','X','*',' ','*',' ','*',' ','*','X','*'},
                {' ','X',' ','X','X','X','X','X','X','X',' '},
                {'*','X','*',' ','*',' ','#',' ','*',' ','*'}}
            },
            {"3551" , new char[,]{
                //1       2       3       4       5       6
                {'*','X','*',' ','*','X','*',' ','#',' ','*'},
                {' ','X',' ','X',' ','X','X','X',' ','X',' '},
                {'*','X','*','X','*','X','*',' ','*','X','*'},
                {' ','X',' ','X',' ','X',' ','X','X','X',' '},
                {'*',' ','*','X','*','X','*','X','*',' ','*'},
                {' ','X','X','X','X','X',' ','X',' ','X','X'},
                {'*',' ','*','X','*',' ','*','X','*','X','*'},
                {'X','X',' ','X',' ','X',' ','X',' ','X',' '},
                {'*',' ','*','X','#','X','*','X','*',' ','*'},
                {' ','X','X','X','X','X',' ','X','X','X',' '},
                {'*',' ','*',' ','*',' ','*','X','*',' ','*'}}
            },
            {"2126" , new char[,]{
                //1       2       3       4       5       6
                {'*',' ','#',' ','*',' ','*','X','*',' ','*'},
                {' ','X','X','X','X','X',' ','X',' ','X',' '},
                {'*','X','*',' ','*','X','*',' ','*','X','*'},
                {' ','X',' ','X','X','X','X','X','X','X',' '},
                {'*',' ','*','X','*',' ','*','X','*',' ','*'},
                {'X','X','X','X',' ','X','X','X',' ','X','X'},
                {'*',' ','*','X','*',' ','*',' ','*','X','*'},
                {' ','X',' ','X',' ','X','X','X','X','X',' '},
                {'*','X','*','X','*',' ','*',' ','*','X','*'},
                {' ','X','X','X','X','X','X','X',' ','X',' '},
                {'*',' ','#',' ','*',' ','*',' ','*',' ','*'}}
            },
            {"3441" , new char[,]{
                //1       2       3       4       5       6
                {'*','X','*',' ','*',' ','#','X','*',' ','*'},
                {' ','X',' ','X','X','X',' ','X',' ','X',' '},
                {'*',' ','*',' ','*','X','*',' ','*','X','*'},
                {' ','X','X','X','X','X','X','X','X','X',' '},
                {'*','X','*',' ','*',' ','*',' ','*','X','*'},
                {' ','X',' ','X','X','X','X','X',' ','X',' '},
                {'*','X','*',' ','#','X','*',' ','*',' ','*'},
                {' ','X','X','X',' ','X','X','X','X','X','X'},
                {'*','X','*','X','*',' ','*',' ','*',' ','*'},
                {' ','X',' ','X','X','X','X','X','X','X','X'},
                {'*',' ','*',' ','*',' ','*',' ','*',' ','*'}}
            },
            {"1532" , new char[,]{
                //1       2       3       4       5       6
                {'*','X','*',' ','*',' ','*',' ','*',' ','*'},
                {' ','X',' ','X','X','X','X','X',' ','X',' '},
                {'*','X','*','X','#',' ','*','X','*','X','*'},
                {' ','X',' ','X',' ','X','X','X',' ','X',' '},
                {'*',' ','*',' ','*','X','*',' ','*','X','*'},
                {' ','X','X','X','X','X',' ','X','X','X',' '},
                {'*','X','*','X','*',' ','*','X','*',' ','*'},
                {' ','X',' ','X',' ','X','X','X','X','X',' '},
                {'#','X','*','X','*','X','*',' ','*','X','*'},
                {' ','X',' ','X',' ','X',' ','X',' ','X','X'},
                {'*',' ','*','X','*',' ','*','X','*',' ','*'}}
            }
        };

        private static List<string[]> solutions = null;

        public static void Start() {
            TTS.Speak("Maze"); //. Give in X-Y

            bool loop = true;

            if (Program.usingVoice) {
                TTS.Speak("Voice input not supported.")Al;
            } else {
                while (loop) {
                    TTS.Speak("First circle.");
                    string first = Console.ReadLine().Replace(" ", "").Substring(0, 2);
                    TTS.Speak("Second circle.");
                    string second = Console.ReadLine().Replace(" ", "").Substring(0, 2);

                    char[,] maze = null;
                    if (mazes.ContainsKey(first + second))
                        maze = mazes[first + second];
                    else if (mazes.ContainsKey(second + first))
                        maze = mazes[second + first];

                    if (maze != null) {
                        TTS.Speak("Maze found.");
                        TTS.Speak("White square.");
                        string white = Console.ReadLine().Substring(0, 3);
                        int whiteX = Int32.Parse(white[0].ToString());
                        int whiteY = Int32.Parse(white[2].ToString());

                        TTS.Speak("Red triangle.");
                        string red = Console.ReadLine().Substring(0, 3);
                        int redX = Int32.Parse(red[0].ToString());
                        int redY = Int32.Parse(red[2].ToString());

                        whiteX = (whiteX - 1) * 2;
                        whiteY = (whiteY - 1) * 2;
                        redX = (redX - 1) * 2;
                        redY = (redY - 1) * 2;

                        solutions = new List<string[]>();
                        aStar(maze, whiteX, whiteY, redX, redY, new string[0]);

                        string[] final = solutions[0];
                        for(int i = 1; i < solutions.Count; i++) {
                            if (solutions[i].Length < final.Length)
                                final = solutions[i];
                        }

                        TTS.Speak("Move " + string.Join(", ", final));

                        loop = false;
                    }
                }
            }
        }

        private static void aStar(char[,] maze, int whiteX, int whiteY, int redX, int redY, string[] from) {
            if (whiteX == redX && whiteY == redY) {
                solutions.Add(from);
            } else {
                string last = from.LastOrDefault();

                if (last != "DOWN" && whiteY > 1 && maze[whiteY - 1, whiteX] != 'X')
                    aStar(maze, whiteX, whiteY - 2, redX, redY, StringArrayAdd(from, "UP"));

                if (last != "LEFT" && whiteX < 9 && maze[whiteY, whiteX + 1] != 'X')
                    aStar(maze, whiteX + 2, whiteY, redX, redY, StringArrayAdd(from, "RIGHT"));

                if (last != "UP" && whiteY < 9 && maze[whiteY + 1, whiteX] != 'X')
                    aStar(maze, whiteX, whiteY + 2, redX, redY, StringArrayAdd(from, "DOWN"));

                if (last != "RIGHT" && whiteX > 1 && maze[whiteY, whiteX - 1] != 'X')
                    aStar(maze, whiteX - 2, whiteY, redX, redY, StringArrayAdd(from, "LEFT"));
            }
        }

        private static string[] StringArrayAdd(string[] from, string add) {
            string[] final = new string[from.Length + 1];
            Array.Copy(from, final, from.Length);
            final[from.Length] = add;

            return final;
        }
    }
}
