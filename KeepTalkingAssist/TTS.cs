﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;

namespace KeepTalkingAssist {
    public static class TTS {

        private static SpeechSynthesizer reader = null;

        private static void Init() {
            reader = new SpeechSynthesizer();
        }

        public static void Speak(string text) {
            if (reader == null)
                Init();

            Console.WriteLine(text);
            reader.Speak(text);
        }

    }
}
