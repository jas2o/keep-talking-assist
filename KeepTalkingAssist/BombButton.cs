﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombButton {

        enum ButtonColor { Blue, White, Yellow, Red };
        enum ButtonText { Abort, Detonate, Hold, Press };

        public static void Start() {
            TTS.Speak("Button.");

            ButtonColor Color = GetColor();
            ButtonText Text = GetText();

            string repeat = Color.ToString() + " " + Text.ToString();
            TTS.Speak(repeat);

            if (Color == ButtonColor.Blue && Text == ButtonText.Abort) {
                ReleasingHeldButtonALL();
            } else if (Bomb.Batteries > 1 && Text == ButtonText.Detonate) {
                TTS.Speak("Press and immediately release.");
            } else if (Color == ButtonColor.White && Bomb.LitLabelCAR == Bomb.ID.Yes) {
                ReleasingHeldButtonALL();
            } else if (Bomb.Batteries > 1 && Bomb.LitLabelFRK == Bomb.ID.Yes) {
                TTS.Speak("Press and immediately release.");
            } else if (Color == ButtonColor.Yellow) {
                ReleasingHeldButtonALL();
            } else if (Color == ButtonColor.Red && Text == ButtonText.Hold) {
                TTS.Speak("Press and immediately release.");
            } else {
                ReleasingHeldButtonALL();
            }
        }

        private static void ReleasingHeldButtonALL() {
            TTS.Speak("Hold the button. Blue four. Yellow five. Other one.");
        }

        private static void ReleasingHeldButton() {
            TTS.Speak("Hold the button");

            ButtonColor Strip = GetColor();

            TTS.Speak(Strip.ToString());

            if (Strip == ButtonColor.Blue) {
                TTS.Speak("Release at a four.");
            } else if (Strip == ButtonColor.White) {
                TTS.Speak("Release at a one.");
            } else if (Strip == ButtonColor.Yellow) {
                TTS.Speak("Release at a five.");
            } else {
                TTS.Speak("Release at a one.");
            }
        }

        private static ButtonColor GetColor() {
            if (Program.usingVoice) {
                SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
                recognizer.SetInputToDefaultAudioDevice();

                Choices choicesColours = new Choices();
                foreach (string color in Enum.GetNames(typeof(ButtonColor)))
                    choicesColours.Add(color);
                GrammarBuilder grammarBuilder = new GrammarBuilder("Color");
                grammarBuilder.Append(choicesColours);
                Grammar grammar = new Grammar(grammarBuilder);
                recognizer.LoadGrammar(grammar);

                RecognitionResult rr = null;
                while (true) {
                    TTS.Speak("Color");
                    rr = recognizer.Recognize();
                    SystemSounds.Beep.Play();
                    if (rr != null) {
                        string colour = rr.Text.ToUpper().Split(' ')[1];
                        if (colour == ButtonColor.Blue.ToString().ToUpper())
                            return ButtonColor.Blue;
                        else if (colour == ButtonColor.White.ToString().ToUpper())
                            return ButtonColor.White;
                        else if (colour == ButtonColor.Yellow.ToString().ToUpper())
                            return ButtonColor.Yellow;
                        else if (colour == ButtonColor.Red.ToString().ToUpper())
                            return ButtonColor.Red;
                    }
                }
            } else {
                while (true) {
                    TTS.Speak("Colour");
                    string colour = Console.ReadLine().ToUpper();
                    colour = colour[0].ToString().ToUpper() + colour.Substring(1);

                    if (colour == ButtonColor.Blue.ToString().ToUpper())
                        return ButtonColor.Blue;
                    else if (colour == ButtonColor.White.ToString().ToUpper())
                        return ButtonColor.White;
                    else if (colour == ButtonColor.Yellow.ToString().ToUpper())
                        return ButtonColor.Yellow;
                    else if (colour == ButtonColor.Red.ToString().ToUpper())
                        return ButtonColor.Red;
                }
            }
        }

        private static ButtonText GetText() {
            if (Program.usingVoice) {
                SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
                recognizer.SetInputToDefaultAudioDevice();

                Choices choicesText = new Choices();
                foreach (string color in Enum.GetNames(typeof(ButtonText)))
                    choicesText.Add(color);
                GrammarBuilder grammarBuilder = new GrammarBuilder("Text");
                grammarBuilder.Append(choicesText);
                Grammar grammar = new Grammar(grammarBuilder);
                recognizer.LoadGrammar(grammar);

                RecognitionResult rr = null;
                while (true) {
                    TTS.Speak("Text");
                    rr = recognizer.Recognize();
                    if (rr != null) {
                        string text = rr.Text.ToUpper().ToUpper().Split(' ')[1];
                        if (text == ButtonText.Abort.ToString().ToUpper())
                            return ButtonText.Abort;
                        else if (text == ButtonText.Detonate.ToString().ToUpper())
                            return ButtonText.Detonate;
                        else if (text == ButtonText.Hold.ToString().ToUpper())
                            return ButtonText.Hold;
                        else if (text == ButtonText.Press.ToString().ToUpper())
                            return ButtonText.Press;
                    }
                }
            } else {
                while (true) {
                    TTS.Speak("Text");
                    string text = Console.ReadLine().ToUpper();
                    text = text[0].ToString().ToUpper() + text.Substring(1);

                    if (text == ButtonText.Abort.ToString().ToUpper())
                        return ButtonText.Abort;
                    else if (text == ButtonText.Detonate.ToString().ToUpper())
                        return ButtonText.Detonate;
                    else if (text == ButtonText.Hold.ToString().ToUpper())
                        return ButtonText.Hold;
                    else if (text == ButtonText.Press.ToString().ToUpper())
                        return ButtonText.Press;
                }
            }
        }
    }
}
