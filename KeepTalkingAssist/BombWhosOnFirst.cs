﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombWhosOnFirst {
        private static List<Label> LeftTop = new List<Label>() {
            new Label(2, "UR"),
        };

        private static List<Label> LeftMiddle = new List<Label>() {
            new Label(3, "YES"),
            new Label(7, "NOTHING"),
            new Label(3, "LED"),
            new Label(8, "THEY ARE"),
        };

        private static List<Label> LeftBottom = new List<Label>() {
            new Label(0, "-"),
            new Label(4, "REED"), //!! Read
            new Label(4, "LEED"), //!! Lead
            new Label(7, "THEY'RE"),
        };

        private static List<Label> RightTop = new List<Label>() {
            new Label(5, "FIRST"),
            new Label(4, "OKAY"),
            new Label(1, "C"),
        };

        private static List<Label> RightMiddle = new List<Label>() {
            new Label(5, "BLANK"),
            new Label(4, "READ"), //!! Reed
            new Label(3, "RED"),
            new Label(3, "YOU"),
            new Label(4, "YOUR"),
            new Label(6, "YOU'RE"),
            new Label(5, "THEIR"), //!! There
        };

        private static List<Label> RightBottom = new List<Label>() {
            new Label(7, "DISPLAY"),
            new Label(4, "SAYS"),
            new Label(2, "NO"),
            new Label(4, "LEAD"), //!! Leed
            new Label(7, "HOLD ON"),
            new Label(7, "YOU ARE"),
            new Label(5, "THERE"), //!! Their
            new Label(3, "SEE"), //Not a problem?
            new Label(3, "CEE"), //Not a problem?
        };

        private class Label {
            public int Length;
            public string Text;

            public Label(int length, string text) {
                Length = length;
                Text = text;
            }
        }

        public static void Start() {
            bool loop = true;

            if (Program.usingVoice) {
                TTS.Speak("Voice input not supported.");
            } else {
                while (loop) {
                    TTS.Speak("Word.");
                    string display = Console.ReadLine().ToUpper();

                    if (display == "EXIT")
                        loop = false;
                    else {

                        if (LeftTop.Any(t => t.Text == display)) {
                            TTS.Speak("Left Top");
                        } else if (RightTop.Any(t => t.Text == display)) {
                            TTS.Speak("Right Top");
                        } else if (LeftMiddle.Any(t => t.Text == display)) {
                            TTS.Speak("Left Middle");
                        } else if (RightMiddle.Any(t => t.Text == display)) {
                            TTS.Speak("Right Middle");
                        } else if (LeftBottom.Any(t => t.Text == display)) {
                            TTS.Speak("Left Bottom");
                        } else if (RightBottom.Any(t => t.Text == display)) {
                            TTS.Speak("Right Bottom");
                        }

                        string button = Console.ReadLine().ToUpper();
                        string[] options = null;

                        if (button == "READY") {
                            options = new string[] { "YES", "OKAY", "WHAT", "MIDDLE", "LEFT", "PRESS", "RIGHT", "BLANK", "READY" /*, "NO", "FIRST", "UHHH", "NOTHING", "WAIT"*/ };
                        } else if (button == "FIRST") {
                            options = new string[] { "LEFT", "OKAY", "YES", "MIDDLE", "NO", "RIGHT", "NOTHING", "UHHH", "WAIT", "READY", "BLANK", "WHAT", "PRESS", "FIRST" };
                        } else if (button == "NO") {
                            options = new string[] { "BLANK", "UHHH", "WAIT", "FIRST", "WHAT", "READY", "RIGHT", "YES", "NOTHING", "LEFT", "PRESS", "OKAY", "NO" /*, "MIDDLE"*/ };
                        } else if (button == "BLANK") {
                            options = new string[] { "WAIT", "RIGHT", "OKAY", "MIDDLE", "BLANK" /*, "PRESS", "READY", "NOTHING", "NO", "WHAT", "LEFT", "UHHH", "YES", "FIRST"*/ };
                        } else if (button == "NOTHING") {
                            options = new string[] { "UHHH", "RIGHT", "OKAY", "MIDDLE", "YES", "BLANK", "NO", "PRESS", "LEFT", "WHAT", "WAIT", "FIRST", "NOTHING" /*, "READY"*/ };
                        } else if (button == "YES") {
                            options = new string[] { "OKAY", "RIGHT", "UHHH", "MIDDLE", "FIRST", "WHAT", "PRESS", "READY", "NOTHING", "YES" /*, "LEFT", "BLANK", "NO", "WAIT"*/ };
                        } else if (button == "WHAT") {
                            options = new string[] { "UHHH", "WHAT" /*, "LEFT", "NOTHING", "READY", "BLANK", "MIDDLE", "NO", "OKAY", "FIRST", "WAIT", "YES", "PRESS", "RIGHT"*/ };
                        } else if (button == "UHHH") {
                            options = new string[] { "READY", "NOTHING", "LEFT", "WHAT", "OKAY", "YES", "RIGHT", "NO", "PRESS", "BLANK", "UHHH" /*, "MIDDLE", "WAIT", "FIRST"*/ };
                        } else if (button == "LEFT") {
                            options = new string[] { "RIGHT", "LEFT" /*, "FIRST", "NO", "MIDDLE", "YES", "BLANK", "WHAT", "UHHH", "WAIT", "PRESS", "READY", "OKAY", "NOTHING"*/ };
                        } else if (button == "RIGHT") {
                            options = new string[] { "YES", "NOTHING", "READY", "PRESS", "NO", "WAIT", "WHAT", "RIGHT" /*, "MIDDLE", "LEFT", "UHHH", "BLANK", "OKAY", "FIRST"*/ };
                        } else if (button == "MIDDLE") {
                            options = new string[] { "BLANK", "READY", "OKAY", "WHAT", "NOTHING", "PRESS", "NO", "WAIT", "LEFT", "MIDDLE" /*, "RIGHT", "FIRST", "UHHH", "YES"*/ };
                        } else if (button == "OKAY") {
                            options = new string[] { "MIDDLE", "NO", "FIRST", "YES", "UHHH", "NOTHING", "WAIT", "OKAY" /*, "LEFT", "READY", "BLANK", "PRESS", "WHAT", "RIGHT"*/ };
                        } else if (button == "WAIT") {
                            options = new string[] { "UHHH", "NO", "BLANK", "OKAY", "YES", "LEFT", "FIRST", "PRESS", "WHAT", "WAIT" /*, "NOTHING", "READY", "RIGHT", "MIDDLE"*/ };
                        } else if (button == "PRESS") {
                            options = new string[] { "RIGHT", "MIDDLE", "YES", "READY", "PRESS" /*, "OKAY", "NOTHING", "UHHH", "BLANK", "LEFT", "FIRST", "WHAT", "NO", "WAIT"*/ };
                        } else if (button == "YOU") {
                            options = new string[] { "SURE", "YOU ARE", "YOUR", "YOU'RE", "NEXT", "UH HUH", "UR", "HOLD", "WHAT?", "YOU" /*, "UH UH", "LIKE", "DONE", "U"*/ };
                        } else if (button == "YOU ARE") {
                            options = new string[] { "YOUR", "NEXT", "LIKE", "UH HUH", "WHAT?", "DONE", "UH UH", "HOLD", "YOU", "U", "YOU'RE", "SURE", "UR", "YOU ARE" };
                        } else if (button == "YOUR") {
                            options = new string[] { "UH UH", "YOU ARE", "UH HUH", "YOUR" /*, "NEXT", "UR", "SURE", "U", "YOU'RE", "YOU", "WHAT?", "HOLD", "LIKE", "DONE"*/ };
                        } else if (button == "YOU'RE") {
                            options = new string[] { "YOU", "YOU'RE" /*, "UR", "NEXT", "UH UH", "YOU ARE", "U", "YOUR", "WHAT?", "UH HUH", "SURE", "DONE", "LIKE", "HOLD"*/ };
                        } else if (button == "UR") {
                            options = new string[] { "DONE", "U", "UR" /*, "UH HUH", "WHAT?", "SURE", "YOUR", "HOLD", "YOU'RE", "LIKE", "NEXT", "UH UH", "YOU ARE", "YOU"*/ };
                        } else if (button == "U") {
                            options = new string[] { "UH HUH", "SURE", "NEXT", "WHAT?", "YOU'RE", "UR", "UH UH", "DONE", "U" /*, "YOU", "LIKE", "HOLD", "YOU ARE", "YOUR"*/ };
                        } else if (button == "UH HUH") {
                            options = new string[] { "UH HUH" /*, "YOUR", "YOU ARE", "YOU", "DONE", "HOLD", "UH UH", "NEXT", "SURE", "LIKE", "YOU'RE", "UR", "U", "WHAT?"*/ };
                        } else if (button == "UH UH") {
                            options = new string[] { "UR", "U", "YOU ARE", "YOU'RE", "NEXT", "UH UH" /*, "DONE", "YOU", "UH HUH", "LIKE", "YOUR", "SURE", "HOLD", "WHAT?"*/ };
                        } else if (button == "WHAT?") {
                            options = new string[] { "YOU", "HOLD", "YOU'RE", "YOUR", "U", "DONE", "UH UH", "LIKE", "YOU ARE", "UH HUH", "UR", "NEXT", "WHAT?" /*, "SURE"*/ };
                        } else if (button == "DONE") {
                            options = new string[] { "SURE", "UH HUH", "NEXT", "WHAT?", "YOUR", "UR", "YOU'RE", "HOLD", "LIKE", "YOU", "U", "YOU ARE", "UH UH", "DONE" };
                        } else if (button == "NEXT") {
                            options = new string[] { "WHAT?", "UH HUH", "UH UH", "YOUR", "HOLD", "SURE", "NEXT" /*, "LIKE", "DONE", "YOU ARE", "UR", "YOU'RE", "U", "YOU"*/ };
                        } else if (button == "HOLD") {
                            options = new string[] { "YOU ARE", "U", "DONE", "UH UH", "YOU", "UR", "SURE", "WHAT?", "YOU'RE", "NEXT", "HOLD" /*, "UH HUH", "YOUR", "LIKE"*/ };
                        } else if (button == "SURE") {
                            options = new string[] { "YOU ARE", "DONE", "LIKE", "YOU'RE", "YOU", "HOLD", "UH HUH", "UR", "SURE" /*, "U", "WHAT?", "NEXT", "YOUR", "UH UH"*/ };
                        } else if (button == "LIKE") {
                            options = new string[] { "YOU'RE", "NEXT", "U", "UR", "HOLD", "DONE", "UH UH", "WHAT?", "UH HUH", "YOU", "LIKE" /*, "SURE", "YOU ARE", "YOUR"*/ };
                        }

                        if (options != null) {
                            string join = String.Join(", ", options);
                            join = join.Replace("?", " question");
                            TTS.Speak(String.Join(", ", options));
                        }

                        //Loop
                    }
                }
            }
        }
    }
}
