﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class Bomb {

        public enum ID { Unknown, Yes, No };

        public enum Serial { Odd, Even };

        public static Serial SerialLast;
        public static ID SerialVowel = ID.Unknown;

        public static int Strikes = 0;
        public static int Batteries = 0;
        public static ID ParallelPort = ID.Unknown;
        public static ID LitLabelCAR = ID.Unknown;
        public static ID LitLabelFRK = ID.Unknown;

        public static void Reset() {
            Strikes = 0;
            Batteries = 0;
            SerialVowel = ID.Unknown;
            ParallelPort = ID.Unknown;
            LitLabelCAR = ID.Unknown;
            LitLabelFRK = ID.Unknown;
        }

        public static void Setup() {
            //The Serial
            TTS.Speak("Serial.");
            {
                TTS.Speak("Is the last digit even?");
                string input = Console.ReadLine().ToUpper();
                if (input == "YES" || input == "ZERO" || input == "0")
                    SerialLast = Serial.Even;
                else
                    SerialLast = Serial.Odd;
            }
            {
                TTS.Speak("Is there a vowel?");
                string input = Console.ReadLine().ToUpper();
                if (input == "YES")
                    SerialVowel = ID.Yes;
                else
                    SerialVowel = ID.No;
            }

            //The rest

            if (Program.usingVoice) {
            } else {
                bool loop = true;
                while (loop) {
                    TTS.Speak("Setup.");
                    string input = Console.ReadLine().ToUpper();
                    string[] word = input.Split(' ');

                    if (input == "DONE") {
                        loop = false;
                    } else if (word[0] == "CAR") {
                        LitLabelCAR = ID.Yes;
                        TTS.Speak("CAR");
                    } else if (word[0] == "FRK" || word[0] == "FREAK") {
                        LitLabelFRK = ID.Yes;
                        TTS.Speak("FREAK");
                    } else if (word[0] == "PARALLEL" || word[0] == "PARA" || word[0] == "PRINTER") {
                        ParallelPort = ID.Yes;
                        TTS.Speak("Parallel Port");
                    } else if (word[0] == "BATTERY" || word[0] == "BATTERIES") {
                        int num = int.Parse(word[1]);
                        Batteries = num;
                        TTS.Speak("BATTERY " + num);
                    }
                }
            }
        }
    }
}
