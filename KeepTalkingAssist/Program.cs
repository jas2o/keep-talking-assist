﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    class Program {
        // Version 1 Revision 3

        public static bool usingVoice = false;

        static void Main(string[] args) {
            //Bomb.Setup();

            if (usingVoice) {
                using (SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"))) {
                    recognizer.SetInputToDefaultAudioDevice();

                    Choices choicesModules = new Choices("Wires", "Button", "Keypad", "Simon", "Words", "Memory", "Morse", "Complex", "Sequence", "Maze", "Password", "Knob");
                    GrammarBuilder grammarBuilder = new GrammarBuilder("Module");
                    grammarBuilder.Append(choicesModules);
                    Grammar grammar = new Grammar(grammarBuilder);
                    grammar.Name = "Modules";

                    recognizer.LoadGrammar(grammar);

                    while (true) {
                        TTS.Speak("Module");
                        RecognitionResult rr = recognizer.Recognize();

                        if (rr != null) {
                            string[] word = rr.Text.ToUpper().Split(' ');
                            if (word[0] == "MODULE") {
                                if (word[1] == "WIRES")
                                    BombWiresSimple.Start();
                                else if (word[1] == "BUTTON")
                                    BombButton.Start();
                            } else {
                                TTS.Speak("Unknown: " + rr.Text);
                            }
                        }
                    }
                }
            } else {
                // Text input
                while (true) {
                    TTS.Speak("Module");
                    string input = Console.ReadLine();
                    string[] word = input.ToUpper().Split(' ');

                    if (word[0] == "SETUP") {
                        Bomb.Setup();
                    } else if (word[0] == "MODULE") {
                        // In the order of being programmed.

                        if (word[1] == "WIRES")
                            BombWiresSimple.Start();
                        else if (word[1] == "BUTTON")
                            BombButton.Start();
                        else if (word[1] == "SIMON")
                            BombSimon.Start();
                        else if (word[1] == "PASSWORD")
                            BombPassword.Start();
                        else if (word[1] == "MEMORY")
                            BombMemory.Start();
                        else if (word[1] == "WORDS" || word[1] == "WHO" || word[1] == "WHO'S")
                            BombWhosOnFirst.Start();
                        else if (word[1] == "KEYPAD" || word[1] == "SYMBOLS")
                            BombKeypad.Start();
                        else if (word[1] == "SEQUENCE")
                            BombWiresSequence.Start();
                        else if (word[1] == "COMPLEX" || word[1] == "COMPLICATED")
                            BombWiresComplex.Start();
                        else if (word[1] == "KNOB")
                            BombKnob.Start();
                        else if (word[1] == "MAZE")
                            BombMaze.Start();
                        else if (word[1] == "MORSE")
                            BombMorseCode.Start();
                    } else {
                        TTS.Speak("Unknown: " + input);
                    }
                }
            }
        }
    }
}
