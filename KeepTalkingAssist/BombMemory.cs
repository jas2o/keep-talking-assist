﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepTalkingAssist {
    public static class BombMemory {

        public static void Start() {
            TTS.Speak("Memory.");

            int stage = 0;

            int[] memoryPositon = new int[5];
            int[] memoryLabel = new int[5];

            while (stage < 5) {
                TTS.Speak("Stage " + (stage+1));
                string input = Console.ReadLine();
                int display = Int32.Parse(input[0].ToString());
                int[] button = new int[] {
                    Int32.Parse(input[1].ToString()),
                    Int32.Parse(input[2].ToString()),
                    Int32.Parse(input[3].ToString()),
                    Int32.Parse(input[4].ToString())
                };

                if (stage == 0) {
                    if (display == 1 || display == 2) {
                        memoryPositon[stage] = 1;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 3) {
                        memoryPositon[stage] = 2;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 4) {
                        memoryPositon[stage] = 3;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    }
                    //End
                } else if (stage == 1) {
                    if (display == 1) {
                        //If the display is 1, press the button labeled "4".
                        memoryLabel[stage] = 4;
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 2 || display == 4) {
                        //If the display is 2, press the button in the same position as you pressed in stage 1.
                        memoryPositon[stage] = memoryPositon[0];
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 3) {
                        //If the display is 3, press the button in the first position.
                        memoryPositon[stage] = 0;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    }
                    //End
                } else if(stage == 2) {
                    if (display == 1) {
                        //If the display is 1, press the button with the same label you pressed in stage 2.
                        memoryLabel[stage] = memoryLabel[1];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 2) {
                        //If the display is 2, press the button with the same label you pressed in stage 1.
                        memoryLabel[stage] = memoryLabel[0];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 3) {
                        //If the display is 3, press the button in the third position.
                        memoryPositon[stage] = 2;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 4) {
                        //If the display is 4, press the button labeled "4".
                        memoryLabel[stage] = 4;
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    }
                    //End
                } else if (stage == 3) {
                    if (display == 1) {
                        //If the display is 1, press the button in the same position as you pressed in stage 1.
                        memoryPositon[stage] = memoryPositon[0];
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 2) {
                        //If the display is 2, press the button in the first position.
                        memoryPositon[stage] = 0;
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    } else if (display == 3 || display == 4) {
                        //If the display is 3, press the button in the same position as you pressed in stage 2.
                        memoryPositon[stage] = memoryPositon[1];
                        memoryLabel[stage] = button[memoryPositon[stage]];
                    }
                    //End
                } else if(stage == 4) {
                    if (display == 1) {
                        //If the display is 1, press the button with the same label you pressed in stage 1.
                        memoryLabel[stage] = memoryLabel[0];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 2) {
                        //If the display is 4, press the button with the same label you pressed in stage 2
                        memoryLabel[stage] = memoryLabel[1];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 3) {
                        //If the display is 4, press the button with the same label you pressed in stage 4.
                        memoryLabel[stage] = memoryLabel[3];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    } else if (display == 4) {
                        //If the display is 4, press the button with the same label you pressed in stage 3.
                        memoryLabel[stage] = memoryLabel[2];
                        for (int i = 0; i < 4; i++) {
                            if (button[i] == memoryLabel[stage]) {
                                memoryPositon[stage] = i;
                                break;
                            }
                        }
                    }
                    //End
                }

                TTS.Speak("Press " + memoryLabel[stage]);

                stage++;
            }
        }

    }
}
